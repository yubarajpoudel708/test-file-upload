package com.yuvi.testapp;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.catalina.connector.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.yuvi.testapp.dao.NewsDao;
import com.yuvi.testapp.dao.NewsDaoImpl;
import com.yuvi.testapp.model.Data;
import com.yuvi.testapp.model.News;
import java.sql.SQLException;


import utils.DB;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	
	private NewsDao newsService = new NewsDaoImpl();
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String listNews(Model model) {
		 
//		ModelAndView modelView = new ModelAndView("news/news_page");
		//List<News> newsList = newsService.listNews();
//		for(News news : newsList) {
//			System.out.println(news.getTitle());
//		}
		model.addAttribute("newsList", newsService.listNews());
		
		return "newsList";
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	public String uploadFile(Model model) {	
		return "upload";
	}
	
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView addNews(Model model) {
		ModelAndView modelView = new ModelAndView("news/news_form");
		News news = new News();
		modelView.addObject("newsForm", news);
		return modelView;
	}
	
	@RequestMapping(value="/list", method =  RequestMethod.GET)
	public String listData(Model model) {
		List<Data> datalist = new ArrayList<Data>();
		try {
			Connection conn = DB.getDbcon();
			String sql = "select * from test";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);		
			while(rs.next()) {
				Data d = new Data(rs.getInt("id"), rs.getString("name"), rs.getString("first"), rs.getString("second"));
				datalist.add(d);
			}
		}catch(Exception e) {
			
		}
		model.addAttribute("datalist", datalist);
		return "datalist";
	}
	
	/**
	 * Upload single file using Spring Controller
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public @ResponseBody
	String uploadFileHandler(@RequestParam("name") String name,
			@RequestParam("first") String firstName,
			@RequestParam("second") String secondName,
			@RequestParam("file") MultipartFile file) {

		if (!file.isEmpty()) {
			PreparedStatement preparedStatement = null;
			Connection conn = null;
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				conn = DB.getDbcon();
				String insertTableSQL = "INSERT INTO test"
						+ "(name, first, second) VALUES"
						+ "(?,?,?)";
				preparedStatement = conn.prepareStatement(insertTableSQL);

				preparedStatement.setString(1, name);
				preparedStatement.setString(2, firstName);
				preparedStatement.setString(3, secondName);

				// execute insert SQL stetement
				preparedStatement.executeUpdate();	
				
				logger.info("Server File Location="
						+ serverFile.getAbsolutePath());
				return "You successfully uploaded file=" + name;
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			} 
			
		} else {
			return "You failed to upload " + name
					+ " because the file was empty.";
		}
	}

//	/**
//	 * Upload multiple file using Spring Controller
//	 */
////	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST)
//	public @ResponseBody
//	String uploadMultipleFileHandler(@RequestParam("name") String[] names,
//			@RequestParam("file") MultipartFile[] files) {
//
//		if (files.length != names.length)
//			return "Mandatory information missing";
//
//		String message = "";
//		for (int i = 0; i < files.length; i++) {
//			MultipartFile file = files[i];
//			String name = names[i];
//			try {
//				byte[] bytes = file.getBytes();
//
//				// Creating the directory to store file
//				String rootPath = System.getProperty("catalina.home");
//				File dir = new File(rootPath + File.separator + "tmpFiles");
//				if (!dir.exists())
//					dir.mkdirs();
//
//				// Create the file on server
//				File serverFile = new File(dir.getAbsolutePath()
//						+ File.separator + name);
//				BufferedOutputStream stream = new BufferedOutputStream(
//						new FileOutputStream(serverFile));
//				stream.write(bytes);
//				stream.close();
//
//				logger.info("Server File Location="
//						+ serverFile.getAbsolutePath());
//
//				message = message + "You successfully uploaded file=" + name
//						+ "<br />";
//			} catch (Exception e) {
//				return "You failed to upload " + name + " => " + e.getMessage();
//			}
//		}
//		return message;
//	}
}
