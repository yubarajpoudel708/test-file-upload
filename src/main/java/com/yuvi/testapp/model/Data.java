package com.yuvi.testapp.model;

public class Data {
	public int id;
	public String image,first,second;
	
	public Data(int id, String image, String first, String second) {
		this.id = id;
		this.image = image;
		this.first = first;
		this.second = second;
	}
	
}
