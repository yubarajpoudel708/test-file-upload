package com.yuvi.testapp.dao;

import com.yuvi.testapp.model.User;

public interface UserDao {
	public void initConnection();
	public void createuser(User user);
	public boolean isValiduser(User user);
}
